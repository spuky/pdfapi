package io.fillpdf.pdfapi;

import com.ocdevel.FillpdfService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/parse")
public class ParseController {

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> parsePdf(@RequestBody Map<String, Object> payload) {
        // Get the Base64 PDF.
        String pdf = payload.get("pdf").toString();

        try {
            FillpdfService fps = new FillpdfService(pdf, "");

            ArrayList<HashMap> fields = fps.parse();

            return ResponseEntity.ok(fields);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

}

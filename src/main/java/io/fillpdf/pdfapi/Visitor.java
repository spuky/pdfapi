package io.fillpdf.pdfapi;

import com.lowagie.text.DocumentException;

import java.io.IOException;

abstract class Visitor {

    public void visit(FieldMapping mapping) throws IOException, DocumentException {
        mapping.accept(this);
    }

    public abstract void visit(TextFieldMapping mapping) throws IOException, DocumentException;

    public abstract void visit(ImageFieldMapping mapping) throws IOException, DocumentException;

}

package io.fillpdf.pdfapi;

import com.lowagie.text.DocumentException;

import java.io.IOException;

public class ImageFieldMapping extends FieldMapping {

    @Override
    public void accept(Visitor v) throws IOException, DocumentException {
        v.visit(this);
    }

}

package io.fillpdf.pdfapi;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocdevel.FillpdfService;
import de.redsix.pdfcompare.CompareResult;
import de.redsix.pdfcompare.PdfComparator;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.*;
import java.util.*;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PdfapiApplicationTests {

    @Autowired
    private MockMvc mvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Test
    public void getHello() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("This route does not do anything.")));
    }

    @Test
    public void postParse() throws Exception {
        Map<String, String> requestFields = new HashMap<>();
        File pdfFile = new File(getClass().getResource("/fillpdf_test_v4.pdf").toURI());
        FileInputStream fisr = new FileInputStream(pdfFile);
        byte[] fileBytes = new byte[(int) pdfFile.length()];
        fisr.read(fileBytes);
        String base64Pdf = Base64.encodeBase64String(fileBytes);
        requestFields.put("pdf", base64Pdf);
        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.post("/api/v1/parse")
                .content(this.json(requestFields))
                .contentType(MediaType.APPLICATION_JSON));
        MvcResult result = resultActions
                .andExpect(status().isOk())
                .andReturn();

        // Convert the JSON from the result into an ArrayList...or try.
        ArrayList<Map<String, Object>> responseFields = this.deserializeParseFields(result.getResponse().getContentAsString());

        // Make sure it parsed out the data we expect.
        for (int f = 0; f < responseFields.size(); f++) {
            Map<String, Object> field = responseFields.get(f);

            // Construct a couple parallel arrays to make the assertions less repetitive.
            List<String> checkKeys = new ArrayList<>();
            checkKeys.add("name");
            checkKeys.add("value");
            checkKeys.add("type");
            List<String> checkValues = new ArrayList<>();
            switch (f) {
                case 0:
                    checkValues.add("List Box6");
                    checkValues.add("ListItem3");
                    checkValues.add("List");
                    break;
                case 1:
                    checkValues.add("List Box7");
                    checkValues.add("");
                    checkValues.add("List");
                    break;
                case 2:
                    checkValues.add("Radio Button3");
                    checkValues.add("Choice 1");
                    checkValues.add("Radiobutton");
                    break;
                case 3:
                    checkValues.add("Signature9");
                    checkValues.add("");
                    checkValues.add("Signature");
                    break;
                case 4:
                    checkValues.add("Radio Button4");
                    checkValues.add("Other Choice 1");
                    checkValues.add("Radiobutton");
                    break;
                case 5:
                    checkValues.add("Text2");
                    checkValues.add("");
                    checkValues.add("Text");
                    break;
                case 6:
                    checkValues.add("Text1");
                    checkValues.add("");
                    checkValues.add("Text");
                    break;
                case 7:
                    checkValues.add("Combo Box8");
                    checkValues.add("Combo1");
                    checkValues.add("Combobox");
                    break;
                case 8:
                    checkValues.add("Button2");
                    checkValues.add("");
                    checkValues.add("Pushbutton");
                    break;
                case 9:
                    checkValues.add("Check Box5");
                    checkValues.add("");
                    checkValues.add("Checkbox");
                    break;
                case 10:
                    checkValues.add("Check Box4");
                    checkValues.add("");
                    checkValues.add("Checkbox");
                    break;
            }

            for (int c = 0; c < checkKeys.size(); c++) {
                assertEquals(field.get("name") + ": expected value for " + checkKeys.get(c) + " matches.", checkValues.get(c), field.get(checkKeys.get(c)));
            }
        }
    }

    @Test
    /**
     * @todo For crying out loud, split this into two tests!
     */
    public void postMerge() throws Exception {
        Map<String, Object> requestFields = new HashMap<>();

        File pdfFile = new File(getClass().getResource("/fillpdf_test_v4.pdf").toURI());
        FileInputStream fisr = new FileInputStream(pdfFile);
        byte[] fileBytes = new byte[(int) pdfFile.length()];
        fisr.read(fileBytes);
        String base64Pdf = Base64.encodeBase64String(fileBytes);
        requestFields.put("pdf", base64Pdf);

        // Fill in a text field and an image field.
        Map<String, Map<String, String>> fields = new HashMap<>();

        Map<String, String> textMapping = new HashMap<>();
        textMapping.put("type", "text");
        textMapping.put("data", "Test text 1");

        fields.put("Text1", textMapping);

        Map<String, String> imageMapping = new HashMap<>();
        File imageFile = new File(getClass().getResource("/testphoto.jpg").toURI());
        FileInputStream fisr2 = new FileInputStream(imageFile);
        byte[] imageFileBytes = new byte[(int) imageFile.length()];
        fisr2.read(imageFileBytes);
        String base64Image = Base64.encodeBase64String(imageFileBytes);
        imageMapping.put("type", "image");
        imageMapping.put("data", base64Image);

        fields.put("Button2", imageMapping);

        requestFields.put("fields", fields);

        String json = this.json(requestFields);
        ResultActions resultActions = mvc.perform(MockMvcRequestBuilders.post("/api/v1/merge")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON));
        MvcResult result = resultActions
                .andExpect(status().isOk())
                .andReturn();

        // Convert the JSON from the result into an ArrayList...or try.
        Map<String, String> responseFields = this.deserializeMergeFields(result.getResponse().getContentAsString());
        String apiPdf = responseFields.get("pdf");
        byte[] apiPdfBytes = Base64.decodeBase64(apiPdf);

        // Process a PDF directly and then check if the Base64 matches.
        FillpdfService merger = new FillpdfService(base64Pdf, "");
        merger.text("Text1", "Test text 1");
        merger.image("Button2", imageFileBytes);
        String directPdf = merger.toByteArray();
        byte[] directPdfBytes = Base64.decodeBase64(directPdf);

        InputStream apiPdfStream = new ByteArrayInputStream(apiPdfBytes);
        InputStream directPdfStream = new ByteArrayInputStream(directPdfBytes);

        final CompareResult comparison = new PdfComparator(directPdfStream, apiPdfStream).compare();
        assertTrue("PDF from API and PDF from us match.", comparison.isEqual());

        // Test flatten. Same thing, basically, but we tell it not to flatten, send the request, and then do the same
        // assertions.
        requestFields.put("flatten", false);
        String json2 = this.json(requestFields);
        ResultActions resultActions2 = mvc.perform(MockMvcRequestBuilders.post("/api/v1/merge")
                .content(json2)
                .contentType(MediaType.APPLICATION_JSON));
        MvcResult result2 = resultActions2
                .andExpect(status().isOk())
                .andReturn();

        // Convert the JSON from the result into an ArrayList...or try.
        Map<String, String> responseFields2 = this.deserializeMergeFields(result2.getResponse().getContentAsString());
        String apiPdf2 = responseFields2.get("pdf");
        byte[] apiPdfBytes2 = Base64.decodeBase64(apiPdf2);

        // Process a PDF directly and then check if the Base64 matches.
        FillpdfService merger2 = new FillpdfService(base64Pdf, "");
        merger2.text("Text1", "Test text 1");
        merger2.image("Button2", imageFileBytes);
        String directPdf2 = merger2.toByteArrayUnflattened();
        byte[] directPdfBytes2 = Base64.decodeBase64(directPdf2);

        InputStream apiPdfStream2 = new ByteArrayInputStream(apiPdfBytes2);
        InputStream directPdfStream2 = new ByteArrayInputStream(directPdfBytes2);

        final CompareResult comparison2 = new PdfComparator(directPdfStream2, apiPdfStream2).compare();
        assertTrue("PDF from API (flatten: false) and PDF from us match.", comparison2.isEqual());
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    protected ArrayList<Map<String, Object>> deserializeParseFields(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, Object> innerObjectType = new HashMap<>();

        ArrayList<Map<String, Object>> fields = objectMapper.readValue(
                json,
                objectMapper.getTypeFactory().constructCollectionType(
                        ArrayList.class, innerObjectType.getClass()
                ));

        return fields;
    }

    protected Map<String, String> deserializeMergeFields(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        TypeReference<HashMap<String, String>> typeRef = new TypeReference<HashMap<String, String>>() {};
        Map<String, String> fields = objectMapper.readValue(json, typeRef);

        return fields;
    }

}
